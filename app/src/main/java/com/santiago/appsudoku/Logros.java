package com.santiago.appsudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.santiago.appsudoku.logica.Datos;

public class Logros extends AppCompatActivity {

    Button btnAtras;
    String[] logros;
    ListView lista;
    ArrayAdapter<String> adaptador;
    Datos datos;
    int sudokusCompletados;
    CheckedTextView elemento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logros);

        //Inicalización de variables
        btnAtras = (Button) findViewById(R.id.botonAtras);
        lista = (ListView) findViewById(R.id.listaLogros);
        datos = (Datos) this.getApplication();
        sudokusCompletados = 0;
        logros = new String[]{"1. Completa 1 Sudoku", "2. Completa un Sudoku en nivel fácil", "3. Completa un Sudoku en nivel normal",
                              "4. Completa un Sudoku en nivel Difícil","5. Completa 5 Sudokus"};
        adaptador = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_checked, logros);
        lista.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        lista.setTextFilterEnabled(true);
        lista.setEnabled(false);
        lista.setAdapter(adaptador);

        //Inicializacion de eventos
        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Logros.this,Inicio.class);
                startActivity(intent);
            }
        });

        verificarLogros();
    }

    public void verificarLogros(){
        sudokusCompletados += datos.getGanadosFacil();
        sudokusCompletados += datos.getGanadosNormal();
        sudokusCompletados += datos.getGanadosDificil();
        if (sudokusCompletados > 0)
            lista.setItemChecked(0,true);
        if (datos.getGanadosFacil() > 0)
            lista.setItemChecked(1,true);
        if (datos.getGanadosNormal() > 0)
            lista.setItemChecked(2,true);
        if (datos.getGanadosDificil() > 0)
            lista.setItemChecked(3,true);
        if (sudokusCompletados >= 5)
            lista.setItemChecked(4,true);
    }
}

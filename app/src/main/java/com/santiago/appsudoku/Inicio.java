package com.santiago.appsudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Inicio extends AppCompatActivity implements View.OnClickListener {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);

        //Inicalización de variables
        Button btn_facil = (Button) findViewById(R.id.botonFacil);
        Button btn_normal = (Button) findViewById(R.id.botonNormal);
        Button btn_dificil = (Button) findViewById(R.id.botonDificil);
        Button btn_instrucciones = (Button) findViewById(R.id.botonInstrucciones);
        Button btn_records = (Button) findViewById(R.id.botonRecords);
        Button btn_logros = (Button) findViewById(R.id.botonLogros);
        Button btn_creditos = (Button) findViewById(R.id.botonCreditos);

        //Inicializacion de eventos
        btn_facil.setOnClickListener(this);
        btn_normal.setOnClickListener(this);
        btn_dificil.setOnClickListener(this);
        btn_instrucciones.setOnClickListener(this);
        btn_records.setOnClickListener(this);
        btn_logros.setOnClickListener(this);
        btn_creditos.setOnClickListener(this);
    }

    public void onClick(View v) {
        procesarEvento(v.getId());
    }

    public void procesarEvento(int opcion){
        Intent intent = null;
        switch (opcion){
            case R.id.botonFacil: ;
                intent = new Intent(Inicio.this,Nivel.class);
                intent.putExtra("nivel",0);
                break;
            case R.id.botonNormal:
                intent = new Intent(Inicio.this,Nivel.class);
                intent.putExtra("nivel",1);
                break;
            case R.id.botonDificil:
                intent = new Intent(Inicio.this,Nivel.class);
                intent.putExtra("nivel",2);
                break;
            case R.id.botonInstrucciones:
                intent = new Intent(Inicio.this,Instrucciones.class);
                break;
            case R.id.botonRecords:
                intent = new Intent(Inicio.this,Records.class);
                break;
            case R.id.botonLogros:
                intent = new Intent(Inicio.this,Logros.class);
                break;
            case R.id.botonCreditos:
                intent = new Intent(Inicio.this,Creditos.class);
                break;
        }
        startActivity(intent);
    }
}

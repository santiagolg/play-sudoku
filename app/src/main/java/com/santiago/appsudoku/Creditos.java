package com.santiago.appsudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Creditos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creditos);

        //Inicalización de variables
        Button btn_atras = (Button) findViewById(R.id.botonAtras);

        //Inicializacion de eventos
        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Creditos.this,Inicio.class);
                startActivity(intent);
            }
        });
    }
}

package com.santiago.appsudoku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.santiago.appsudoku.logica.Datos;

public class Records extends AppCompatActivity {

    Button btnAtras;
    TextView recordFacil, recordNormal, recordDificil, ganadasFacil, ganadasNormal, ganadasDificil;
    Datos datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);

        //Inicalización de variables
        btnAtras = (Button) findViewById(R.id.botonAtras);
        recordFacil = (TextView) findViewById(R.id.tiempoRecordFacil);
        recordNormal = (TextView) findViewById(R.id.tiempoRecordNormal);
        recordDificil = (TextView) findViewById(R.id.tiempoRecordDificil);
        ganadasFacil = (TextView) findViewById(R.id.numeroCompletosFacil);
        ganadasNormal = (TextView) findViewById(R.id.numeroCompletosNormal);
        ganadasDificil = (TextView) findViewById(R.id.numeroCompletosDificil);
        datos = (Datos)this.getApplication();




        //Inicializacion de eventos
        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Records.this,Inicio.class);
                startActivity(intent);
            }
        });

        actualizarDatos();
    }

    public void actualizarDatos(){
        if(datos.getRecordFacil() != null)
            recordFacil.setText(datos.getRecordFacil());
        if(datos.getRecordNormal() != null)
            recordNormal.setText(datos.getRecordNormal());
        if(datos.getRecordDificil() != null)
            recordDificil.setText(datos.getRecordDificil());
        ganadasFacil.setText(Integer.toString(datos.getGanadosFacil()));
        ganadasNormal.setText(Integer.toString(datos.getGanadosNormal()));
        ganadasDificil.setText(Integer.toString(datos.getGanadosDificil()));
    }
}

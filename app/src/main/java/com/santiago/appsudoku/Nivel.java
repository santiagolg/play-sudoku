package com.santiago.appsudoku;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.santiago.appsudoku.logica.Datos;
import com.santiago.appsudoku.logica.InventarioSudokus;
import com.santiago.appsudoku.logica.Sudoku;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalTime;

public class Nivel extends AppCompatActivity implements View.OnClickListener {

    InventarioSudokus inventario;
    Sudoku sudoku;
    Datos datos;
    byte[][] sudokuJuego, sudokuSolucion;
    GridLayout gridSudoku, teclado;
    Button btn_atras, botonSeleccionado, botonTeclado, botonVerificar, botonActual;
    int size, fuente, contador, nivel;
    View celdaActual;
    boolean completo;
    String textoBoton;
    Chronometer cronometro;
    TextView record;
    long tiempoActual, tiempoRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel);

        //Inicalización de variables
        nivel = getIntent().getIntExtra("nivel",0);
        btn_atras = (Button) findViewById(R.id.botonAtras);
        botonVerificar = (Button) findViewById(R.id.botonVerificar);
        datos = (Datos)this.getApplication();
        cronometro = (Chronometer) findViewById(R.id.cronometro);
        record = (TextView) findViewById(R.id.tiempoRecord);
        record.setText(datos.getRecordNivel(nivel));
        inventario = new InventarioSudokus();
        sudoku = inventario.sudokuAleatorio(nivel);
        sudokuJuego = sudoku.getSudoku();
        sudokuSolucion = sudoku.getSolucion();

        iniciarSudoku();
        crearTeclado();
        cronometro.start();


        //Inicializacion de eventos
        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Nivel.this,Inicio.class);
                startActivity(intent);
            }
        });

        botonVerificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numeroCelda = 0;
                completo = true;
                for (int i = 0; i<9 ;i++){
                    for (int j=0; j<9;j++){
                        botonActual = (Button) findViewById(numeroCelda);
                        textoBoton = (String) botonActual.getText();
                        if (textoBoton == "" || Byte.parseByte(textoBoton) != sudokuSolucion[i][j]){
                            completo = false;
                            break;
                        }
                        numeroCelda++;
                    }
                }
                if (completo){
                    Toast.makeText(getApplicationContext(),"¡Sudoku completado!", Toast.LENGTH_SHORT).show();
                    cronometro.stop();
                    procesarDatos();
                }else {
                    Toast.makeText(getApplicationContext(),"Sudoku incorrecto", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void procesarDatos(){
        datos.aumentarGanados(nivel);
        if (datos.getRecordNivel(nivel) == null){
            datos.setTiempoNivel(nivel, cronometro.getBase());
            datos.setRecordNivel(nivel,(String) cronometro.getText());
            record.setText(cronometro.getText());
        }else{
            tiempoActual = SystemClock.elapsedRealtime() - cronometro.getBase();
            if(tiempoActual < datos.getTiempoNivel(nivel)){
                datos.setTiempoNivel(nivel,cronometro.getBase());
                datos.setRecordNivel(nivel,(String) cronometro.getText());
                record.setText(cronometro.getText());
            }
        }

    }

    public  void  crearTeclado(){
        teclado = (GridLayout) findViewById(R.id.teclado);
        GridLayout.LayoutParams lp;
        int contadorBoton;
        int ancho = getBaseContext().getResources().getDisplayMetrics().widthPixels;
        Button btn;
        contadorBoton = 0;

        for(int i= 0;i<teclado.getColumnCount();i++)
        {
            lp = new GridLayout.LayoutParams(GridLayout.spec(0), GridLayout.spec(i));
            lp.width = ancho/9;
            lp.height = 60;
            btn = new Button(this);
            btn.setOnClickListener(this);
            btn.setId(contador);
            contador++;
            btn.setText(getNumero(contadorBoton++));
            btn.setTextSize(fuente);
            teclado.addView(btn,lp);
        }
    }

    public String getNumero(int i){

        return "123456789".substring(i, i+1);
    }

    public void iniciarSudoku(){
        gridSudoku = (GridLayout) findViewById(R.id.gridSudoku);
        GridLayout.LayoutParams lp;
        contador  = 0;
        int ancho = getBaseContext().getResources().getDisplayMetrics().widthPixels;
        Button btn;
        fuente = 13;
        for(int i= 0;i<gridSudoku.getRowCount();i++)
        {
            for(int j=0;j<gridSudoku.getColumnCount();j++)
            {
                lp = new GridLayout.LayoutParams(GridLayout.spec(i), GridLayout.spec(j));;
                lp.width = ancho/9;
                lp.height = 53;
                btn = new Button(this);
                btn.setOnClickListener(this);
                btn.setId(contador);
                contador++;
                if (sudokuJuego[i][j] != 0) {
                    btn.setText(Byte.toString(sudokuJuego[i][j]));
                    btn.setClickable(false);
                }
                btn.setTextSize(fuente);
                btn.setBackgroundColor(Color.TRANSPARENT);
                gridSudoku.addView(btn,lp);
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View view) {
        if(view.getId() < 81) {
            botonSeleccionado = (Button) view;
            for (int i = 0; i < 81; i++) {
                celdaActual = findViewById(i);
                celdaActual.setEnabled(false);
            }
            botonSeleccionado.setEnabled(true);
        } else{
            botonTeclado = (Button)view;
            botonSeleccionado.setText((CharSequence) botonTeclado.getText());
            for (int i = 0; i < 81; i++) {
                celdaActual = findViewById(i);
                celdaActual.setEnabled(true);
            }
        }
    }

}

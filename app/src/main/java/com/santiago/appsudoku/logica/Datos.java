package com.santiago.appsudoku.logica;

import android.app.Application;

/**
 * Created by Santiago on 26/10/2017.
 */

public class Datos extends Application {

    private long tiempoFacil;
    private long tiempoNormal;
    private long tiempoDificil;
    private String recordFacil;
    private String recordNormal;
    private String recordDificil;
    private int ganadosFacil;
    private int ganadosNormal;
    private int ganadosDificil;

    public long getTiempoNivel(int nivel){
        if (nivel == 0){
            return tiempoFacil;
        }else if (nivel == 1){
            return tiempoNormal;
        }else {
            return tiempoDificil;
        }
    }

    public void setTiempoNivel(int nivel, long tiempo){
        if (nivel == 0){
            tiempoFacil = tiempo;
        }else if (nivel == 1){
            tiempoNormal = tiempo;
        }else {
            tiempoDificil = tiempo;
        }
    }

    public String  getRecordNivel(int nivel){
        if (nivel == 0){
            return recordFacil;
        }else if (nivel == 1){
            return recordNormal;
        }else {
            return recordDificil;
        }
    }

    public void setRecordNivel(int nivel, String record){
        if (nivel == 0){
            recordFacil = record;
        }else if (nivel == 1){
            recordNormal = record;
        }else {
            recordDificil = record;
        }
    }

    public long getTiempoFacil() {
        return tiempoFacil;
    }

    public void setTiempoFacil(long tiempoFacil) {
        this.tiempoFacil = tiempoFacil;
    }

    public long getTiempoNormal() {
        return tiempoNormal;
    }

    public void setTiempoNormal(long tiempoNormal) {
        this.tiempoNormal = tiempoNormal;
    }

    public long getTiempoDificil() {
        return tiempoDificil;
    }

    public void setTiempoDificil(long tiempoDificil) {
        this.tiempoDificil = tiempoDificil;
    }

    public String getRecordFacil() {
        return recordFacil;
    }

    public void setRecordFacil(String recordFacil) {
        this.recordFacil = recordFacil;
    }

    public String getRecordNormal() {
        return recordNormal;
    }

    public void setRecordNormal(String recordNormal) {
        this.recordNormal = recordNormal;
    }

    public String getRecordDificil() {
        return recordDificil;
    }

    public void setRecordDificil(String recordDificil) {
        this.recordDificil = recordDificil;
    }

    public int getGanadosFacil() {
        return ganadosFacil;
    }

    public void setGanadosFacil(int ganadosFacil) {
        this.ganadosFacil = ganadosFacil;
    }

    public int getGanadosNormal() {
        return ganadosNormal;
    }

    public void setGanadosNormal(int ganadosNormal) {
        this.ganadosNormal = ganadosNormal;
    }

    public int getGanadosDificil() {
        return ganadosDificil;
    }

    public void setGanadosDificil(int ganadosDificil) {
        this.ganadosDificil = ganadosDificil;
    }

    public void aumentarGanados(int nivel){
        if (nivel == 0){
            ganadosFacil++;
        }else if (nivel == 1){
            ganadosNormal++;
        }else {
            ganadosDificil++;
        }
    }
}

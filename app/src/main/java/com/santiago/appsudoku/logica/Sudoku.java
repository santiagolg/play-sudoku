package com.santiago.appsudoku.logica;

/**
 * Created by Santiago on 21/10/2017.
 */

public class Sudoku {

    private byte sudoku[][];
    private byte solucion[][];

    Sudoku(byte sudoku[][], byte solucion[][]){
        this.sudoku = sudoku;
        this.solucion = solucion;
    }

    public byte[][] getSudoku() {
        return sudoku;
    }

    public byte[][] getSolucion() {
        return solucion;
    }
}
